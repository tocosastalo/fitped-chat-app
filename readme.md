# FITPED Assignment - Chat application

## Description
Development of the frontend using JavaScript framework [Vue.js](https://vuejs.org/).

The main task of the student is the implementation of the user interface according to the following assignments using Vue.js framework. The application will communicate with the server via the REST API interface, which is already prepared and described in the `/server` folder.

Application will allow user registration, login, room creation and chatting.

## Documentation
Documentation is available at [drive.google.com](https://drive.google.com/file/d/1KX0lo7BCbAnfL4r2oBzjzVPdRxBRaBsR/view?usp=sharing).

## Contact
tomas.jakubek@mendelu.cz