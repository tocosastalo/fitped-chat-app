## Description

NodeJS backend for chat app created using [Nest](https://github.com/nestjs/nest) framework.

The server is not connected to any database. All data is stored in memory only and is lost when the server is turned off.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# production mode
$ npm run start:prod
```

## API URL
By default, the server starts at the address [http://localhost:3333](http://localhost:3333).


## Swagger documentation

If the server is running, you can view the Swagger API documentation at [http://localhost:3333/api-docs/](http://localhost:3333/api-docs/).

## Mocked data

One test user is created in the application. You can use him to log in to the application.

```json
  {
    email: 'admin@example.com',
    password: 'admin',
  },
```

There are also 2 test chat rooms - `Boys&Girls room` and `Gitter`.
