import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class ServerErrorDtoGet {
  @ApiPropertyOptional()
  error?: string;

  @ApiProperty()
  message: string;

  @ApiProperty()
  statusCode: number;
}
