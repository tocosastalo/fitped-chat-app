import { Request } from 'express';

export enum Gender {
  male = 'male',
  female = 'female',
  other = 'other',
}

export interface UserEntity {
  id: string;
  email: string;
  name: string;
  surname: string;
  gender: Gender;
  password: string;
}

export interface MessageEntity {
  id: string;
  userFromId: string;
  created: Date;
  message: string;
}

export interface RoomEntity {
  id: string;
  created: Date;
  title: string;
  description: string;
  ownerId: string;
  activeUsers: Array<{ userId: string; lastActive: Date }>;
  messages: MessageEntity[];
}

export interface DBData {
  users: UserEntity[];
  rooms: RoomEntity[];
}

export type RequestWithUser = Request & { user: UserEntity };
