import * as cls from 'cls-hooked';
import { Request } from 'express';
import { RequestWithUser, UserEntity } from '../types';

export class RequestContext {
  public static nsid = 'random_NAV_guid';
  public readonly id: number;
  public request: Request | RequestWithUser;
  public response: Response;

  constructor(request: Request | RequestWithUser, response: Response) {
    this.id = Math.random();
    this.request = request;
    this.response = response;
  }

  public static currentRequestContext(): RequestContext | null {
    const session = cls.getNamespace(RequestContext.nsid);
    if (session && session.active) {
      return session.get(RequestContext.name);
    }

    return null;
  }

  public static currentRequest(): Request | RequestWithUser | null {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      return requestContext.request;
    }

    return null;
  }

  public static currentUser() {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      const user: UserEntity | null | any =
        requestContext.request['user'] || null;
      return user;
    }

    return null;
  }
}
