import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ServerErrorDtoGet } from '../../shared/dtos/serverError.dto';
import { JwtAuthGuard } from '../../shared/guards/jwt-auth.guard';
import { RequestWithUser } from '../../shared/types';
import { UserDtoGet } from '../users/dto/getUser';
import { CreateMessageDtoPost } from './dto/createMessage.dto';
import { CreateRoomDtoPost } from './dto/createRoom.dto';
import { MessageDtoGet } from './dto/getMessage.dto';
import { RoomDtoGet } from './dto/getRoom.dto';
import { RoomsService } from './rooms.service';

@Controller('rooms')
@ApiTags('Rooms')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class RoomsController {
  constructor(private roomsService: RoomsService) {}

  @Post()
  @ApiResponse({
    status: 200,
    type: RoomDtoGet,
  })
  @ApiOperation({ summary: 'Create new room' })
  async createNewRoom(@Body() dto: CreateRoomDtoPost) {
    return this.roomsService.createNewRoom(dto);
  }

  @Get()
  @ApiResponse({
    status: 201,
    type: RoomDtoGet,
    isArray: true,
  })
  @ApiOperation({ summary: 'Get all rooms' })
  async getAllRooms() {
    return this.roomsService.getAllRooms();
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    description: 'Room ID',
    type: String,
  })
  @ApiResponse({
    status: 201,
    type: RoomDtoGet,
  })
  @ApiResponse({
    status: 404,
    type: ServerErrorDtoGet,
  })
  @ApiOperation({ summary: 'Get room by ID' })
  async getOneRoom(@Param('id') id: string) {
    return this.roomsService.getRoomById(id);
  }

  @Post(':id/messages')
  @ApiParam({
    name: 'id',
    description: 'Room ID',
    type: String,
  })
  @ApiResponse({
    status: 200,
    type: MessageDtoGet,
  })
  @ApiResponse({
    status: 404,
    type: ServerErrorDtoGet,
  })
  @ApiOperation({ summary: 'Create new message in room' })
  async createRoomMessage(
    @Param('id') id: string,
    @Body() dto: CreateMessageDtoPost,
    @Request() req: RequestWithUser,
  ) {
    return this.roomsService.createRoomMessage(id, dto, req.user);
  }

  @Get(':id/messages')
  @ApiParam({
    name: 'id',
    description: 'Room ID',
    type: String,
  })
  @ApiResponse({
    status: 201,
    type: MessageDtoGet,
    isArray: true,
  })
  @ApiResponse({
    status: 404,
    type: ServerErrorDtoGet,
  })
  @ApiOperation({ summary: 'Get messages in room' })
  async getRoomMessages(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    return this.roomsService.getRoomMessages(id, req.user);
  }

  @Get(':id/users')
  @ApiParam({
    name: 'id',
    description: 'Room ID',
    type: String,
  })
  @ApiResponse({
    status: 201,
    type: UserDtoGet,
    isArray: true,
  })
  @ApiResponse({
    status: 404,
    type: ServerErrorDtoGet,
  })
  @ApiOperation({ summary: 'Get active users in room' })
  async getRoomUsers(@Param('id') id: string) {
    return this.roomsService.getRoomUsers(id);
  }
}
