import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { CreateRoomDtoPost } from './dto/createRoom.dto';
import { db } from '../db';
import { MessageEntity, RoomEntity, UserEntity } from '../../shared/types';
import { RoomDtoGet } from './dto/getRoom.dto';
import { MessageDtoGet } from './dto/getMessage.dto';
import { CreateMessageDtoPost } from './dto/createMessage.dto';
import { differenceInSeconds } from 'date-fns';
import { UserDtoGet } from '../users/dto/getUser';

@Injectable()
export class RoomsService {
  constructor() {
    setInterval(this.checkActiveUsersInRooms, 4000);
  }

  public getAllRooms(): RoomDtoGet[] {
    return db.data.rooms.map(this.mapRoomEntityToDtoGet);
  }

  public getRoomById(id: string): RoomDtoGet {
    const room = this.findRoomById(id);
    return this.mapRoomEntityToDtoGet(room);
  }

  public createNewRoom(dto: CreateRoomDtoPost): RoomDtoGet {
    const newRoom: RoomEntity = {
      ...dto,
      created: new Date(),
      messages: [],
      activeUsers: [],
      id: uuidv4(),
      ownerId: '123',
    };

    db.operations.addNewRoom(newRoom);

    return this.mapRoomEntityToDtoGet(newRoom);
  }

  public createRoomMessage(
    roomId: string,
    dto: CreateMessageDtoPost,
    user: UserEntity,
  ): MessageDtoGet {
    this.findRoomById(roomId);

    const newMessage: MessageEntity = {
      ...dto,
      created: new Date(),
      id: uuidv4(),
      userFromId: user.id,
    };

    db.operations.addRoomMessage(roomId, newMessage);

    return this.mapMessageEntityToDtoGet(newMessage);
  }

  public getRoomMessages(id: string, user: UserEntity): MessageDtoGet[] {
    const room = this.findRoomById(id);
    db.operations.updateUserRoomActivity(id, user.id);
    return room.messages.map(this.mapMessageEntityToDtoGet);
  }

  public getRoomUsers(id: string): UserDtoGet[] {
    const room = this.findRoomById(id);
    return room.activeUsers
      .map(({ userId }) => db.operations.findUserById(userId))
      .filter((user) => user != null)
      .map(({ email, gender, id, name, surname }) => ({
        email,
        id,
        name,
        surname,
        gender,
      }));
  }

  public findRoomById(id: string): RoomEntity {
    const room = db.operations.findRoomById(id);
    if (room == null) {
      throw new NotFoundException("This room doesn't exist.");
    }

    return room;
  }

  private checkActiveUsersInRooms() {
    const now = new Date();

    db.data.rooms.forEach(({ activeUsers, id }) => {
      activeUsers.forEach(({ lastActive, userId }) => {
        if (differenceInSeconds(now, lastActive) > 1) {
          db.operations.removeUserFromRoom(id, userId);
        }
      });
    });
  }

  private mapRoomEntityToDtoGet({
    created,
    description,
    id,
    messages,
    ownerId,
    title,
    activeUsers,
  }: RoomEntity): RoomDtoGet {
    return {
      created,
      description,
      id,
      ownerId,
      title,
      totalMessages: messages.length,
      totalUsers: activeUsers.length,
    };
  }

  private mapMessageEntityToDtoGet({
    created,
    id,
    message,
    userFromId,
  }: MessageEntity): MessageDtoGet {
    const user = db.operations.findUserById(userFromId);
    return {
      created,
      id,
      message,
      userEmail: user?.email ?? 'unknown email',
      userFullName: user != null ? `${user.name} ${user.surname}` : 'Anonym',
      userId: user?.id ?? 'unknown',
    };
  }
}
