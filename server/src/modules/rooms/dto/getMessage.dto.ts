import { ApiProperty } from '@nestjs/swagger';

export class MessageDtoGet {
  @ApiProperty()
  id: string;

  @ApiProperty()
  message: string;

  @ApiProperty()
  created: Date;

  @ApiProperty()
  userFullName: string;

  @ApiProperty()
  userId: string;

  @ApiProperty()
  userEmail: string;
}
