import { ApiProperty } from '@nestjs/swagger';

export class RoomDtoGet {
  @ApiProperty()
  title: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  id: string;

  @ApiProperty()
  created: Date;

  @ApiProperty()
  ownerId: string;

  @ApiProperty()
  totalUsers: number;

  @ApiProperty()
  totalMessages: number;
}
