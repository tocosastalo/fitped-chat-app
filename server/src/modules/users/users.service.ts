import { Injectable, ConflictException } from '@nestjs/common';
import { pbkdf2Sync, randomBytes } from 'crypto';
import { v4 as uuidv4 } from 'uuid';
import { UserEntity } from '../../shared/types';
import { db } from '../db';
import { CreateUserDtoPost } from './dto/createUser.dto';
import { UserDtoGet } from './dto/getUser';

@Injectable()
export class UsersService {
  public createNew(dto: CreateUserDtoPost): UserDtoGet {
    const existedUser = db.operations.findUserByEmail(dto.email);
    if (existedUser != null) {
      throw new ConflictException(
        'This email address is already registered. Try another.',
      );
    }

    const newUser: UserEntity = {
      ...dto,
      id: uuidv4(),
      password: this.hashPassword(dto.password),
    };

    db.operations.addNewUser(newUser);

    const { password, ...rest } = newUser;

    return {
      ...rest,
    };
  }

  public getById(id: string): UserEntity | undefined {
    return db.operations.findUserById(id);
  }

  public getByEmail(email: string): UserEntity | undefined {
    return db.operations.findUserByEmail(email);
  }

  private hashPassword(password: string) {
    const salt = randomBytes(16).toString('hex');
    const hash = pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');
    return [salt, hash].join('$');
  }
}
