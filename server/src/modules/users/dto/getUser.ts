import { ApiProperty } from '@nestjs/swagger';
import { Gender } from '../../../shared/types';

export class UserDtoGet {
  @ApiProperty()
  id: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  surname: string;

  @ApiProperty({ enum: Gender, enumName: 'Gender' })
  gender: Gender;
}
