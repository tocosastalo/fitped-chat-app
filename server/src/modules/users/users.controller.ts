import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateUserDtoPost } from './dto/createUser.dto';
import { UserDtoGet } from './dto/getUser';
import { UsersService } from './users.service';

@Controller('users')
@ApiTags('Users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post()
  @ApiResponse({
    status: 200,
    type: UserDtoGet,
  })
  @ApiOperation({ summary: 'Create new user' })
  async createNewRoom(@Body() dto: CreateUserDtoPost) {
    return this.usersService.createNew(dto);
  }
}
