import { v4 as uuidv4 } from 'uuid';
import {
  DBData,
  Gender,
  MessageEntity,
  RoomEntity,
  UserEntity,
} from '../../shared/types';

const findUserByEmail = (email: string) =>
  db.data.users.find((u) => u.email === email);

const findUserById = (id: string) => db.data.users.find((u) => u.id === id);

const findRoomById = (id: string) => db.data.rooms.find((r) => r.id === id);

const addNewRoom = (newRoom: RoomEntity) =>
  (db.data.rooms = db.data.rooms.concat(newRoom));

const addNewUser = (newUser: UserEntity) =>
  (db.data.users = db.data.users.concat(newUser));

const addRoomMessage = (roomId: string, newMessage: MessageEntity) => {
  const index = db.data.rooms.findIndex((room) => room.id === roomId);
  if (index < 0) {
    return;
  }
  db.data.rooms[index].messages.push(newMessage);
};

const updateUserRoomActivity = (roomId: string, userId) => {
  const roomIndex = db.data.rooms.findIndex((room) => room.id === roomId);
  if (roomIndex < 0) {
    return;
  }

  const now = new Date();
  const userIndex = db.data.rooms[roomIndex].activeUsers.findIndex(
    (user) => user.userId === userId,
  );

  if (userIndex < 0) {
    db.data.rooms[roomIndex].activeUsers.push({ lastActive: now, userId });
  } else {
    db.data.rooms[roomIndex].activeUsers[userIndex] = {
      lastActive: now,
      userId,
    };
  }
};

const removeUserFromRoom = (roomId: string, userId: string) => {
  const index = db.data.rooms.findIndex((room) => room.id === roomId);
  if (index < 0) {
    return;
  }
  db.data.rooms[index].activeUsers = db.data.rooms[index].activeUsers.filter(
    (user) => user.userId !== userId,
  );
};

const users: UserEntity[] = [
  {
    id: uuidv4(),
    email: 'admin@example.com',
    gender: Gender.male,
    name: 'Super',
    surname: 'Admin',
    password:
      'a0f5c2e4651c44d577d2ad046e2bcfcb$5ee6e2f7dca774df89aa63f06641611280bceaf3622c1ccc4ee73ca5d41a8975', // admin
  },
];

const rooms: RoomEntity[] = [
  {
    id: uuidv4(),
    created: new Date(),
    ownerId: users[0].id,
    messages: [
      {
        created: new Date(),
        id: uuidv4(),
        message: 'Test message',
        userFromId: users[0].id,
      },
      {
        created: new Date(),
        id: uuidv4(),
        message: 'Hello everyone',
        userFromId: users[0].id,
      },
    ],
    activeUsers: [],
    title: 'Boys&Girls room',
    description: 'Room for big boys and girls. Yeh.',
  },
  {
    id: uuidv4(),
    created: new Date(),
    ownerId: users[0].id,
    messages: [],
    activeUsers: [],
    title: 'Gitter',
    description:
      'Gitter is a chat and networking platform that helps to manage, grow and connect communities through messaging, content and discovery.',
  },
];

export const db = {
  data: {
    users,
    rooms,
  } as DBData,
  operations: {
    findUserByEmail,
    findUserById,
    findRoomById,
    addNewRoom,
    addNewUser,
    addRoomMessage,
    updateUserRoomActivity,
    removeUserFromRoom,
  },
};
