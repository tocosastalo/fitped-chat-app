import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class LoginDtoPost {
  @ApiProperty({ example: 'admin@example.com' })
  @IsString()
  email: string;

  @ApiProperty({ example: 'admin' })
  @IsString()
  password: string;
}
