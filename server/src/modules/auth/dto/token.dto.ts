import { ApiProperty } from '@nestjs/swagger';

export class TokenDtoGet {
  @ApiProperty()
  token: string;
}
