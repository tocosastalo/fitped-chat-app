import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { pbkdf2Sync } from 'crypto';
import { db } from '../db';
import { LoginDtoPost } from './dto/login.dto';
import { TokenDtoGet } from './dto/token.dto';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  public login({ email, password }: LoginDtoPost): TokenDtoGet {
    const user = db.operations.findUserByEmail(email);

    if (user == null || !this.verifyHash(password, user.password)) {
      throw new UnauthorizedException('Invalid login credentials.');
    }

    const { id, name, surname } = user;
    const token = this.jwtService.sign({
      email,
      sub: id,
      name,
      surname,
    });

    return {
      token,
    };
  }

  private verifyHash(password: string, hashedPassword: string) {
    const originalHash = hashedPassword.split('$')[1];
    const salt = hashedPassword.split('$')[0];
    const hash = pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');

    return hash === originalHash;
  }
}
