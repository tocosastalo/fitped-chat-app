import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ServerErrorDtoGet } from '../../shared/dtos/serverError.dto';
import { AuthService } from './auth.service';
import { LoginDtoPost } from './dto/login.dto';
import { TokenDtoGet } from './dto/token.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  @ApiResponse({
    status: 201,
    type: TokenDtoGet,
  })
  @ApiResponse({
    status: 401,
    type: ServerErrorDtoGet,
  })
  @ApiOperation({ summary: 'Login user' })
  async login(@Body() dto: LoginDtoPost) {
    return this.authService.login(dto);
  }
}
