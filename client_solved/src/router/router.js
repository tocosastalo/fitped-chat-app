import Vue from "vue";
import VueRouter from "vue-router";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Home from "./views/Home.vue";
import NotFound from "./views/NotFound.vue";
import Rooms from "./views/Rooms.vue";
import RoomDetail from "./views/RoomDetail.vue";
import { tokenManager } from "../main";

Vue.use(VueRouter);

const routes = [
  { path: "/login", component: Login, name: "login" },
  { path: "/register", component: Register, name: "register" },
  { path: "/rooms", component: Rooms, name: "rooms", meta: { requiresAuth: true } },
  { path: "/rooms/:id", component: RoomDetail, name: "roomDetail", meta: { requiresAuth: true } },
  { path: "", component: Home, name: "home" },
  { path: "*", component: NotFound, name: "notFound" },
];

const router = new VueRouter({
  routes,
  mode: "history",
});

router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.requiresAuth) {
    if (tokenManager.isUserLogged()) {
      next();
    } else {
      next({ name: "login" });
    }
  } else {
    next();
  }
});

export default router;
