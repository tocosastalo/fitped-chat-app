import { axiosInstance } from "./http-common";
import { LS_TOKEN_KEY } from "./constants";

export class TokenManager {
  token = null;

  setToken(token) {
    this.token = token;
    axiosInstance.defaults.headers["Authorization"] = `Bearer ${token}`;
    localStorage.setItem(LS_TOKEN_KEY, token);
  }

  logout() {
    this.token = null;
    delete axiosInstance.defaults.headers["Authorization"];
    localStorage.removeItem(LS_TOKEN_KEY);
  }

  renew() {
    const token = localStorage.getItem(LS_TOKEN_KEY);
    if (token) {
      this.setToken(token);
    }
  }

  getPayload() {
    if (this.token) {
      const parts = this.token.split(".");
      const rawToken = decodeURIComponent(escape(atob(parts[1])));
      return JSON.parse(rawToken);
    }
    return null;
  }

  isUserLogged() {
    return this.token != null;
  }
}
